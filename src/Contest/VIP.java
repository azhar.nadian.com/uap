package Contest;

class VIP extends TiketKonser {
    //Kelas VIP dideklarasikan dan meng-extend kelas TiketKonser menggunakan kata kunci 
    //extends. Ini berarti bahwa kelas VIP mewarisi properti dan metode dari kelas TiketKonser

    public VIP(String nama, double harga) {
        super(nama, harga);
    }

    //@Override digunakan untuk menandakan bahwa metode berikutnya merupakan override 
    //dari metode dalam superclass. Dalam kasus ini, metode hitungHarga() di-override

    @Override
    public double hitungHarga() {
        return harga;
    }
}

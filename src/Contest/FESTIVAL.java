package Contest;

class FESTIVAL extends TiketKonser {
    //Kelas FESTIVAL dideklarasikan dan meng-extend kelas TiketKonser menggunakan kata kunci 
    //extends. Ini berarti bahwa kelas FESTIVAL mewarisi properti dan metode dari kelas TiketKonser
    
    public FESTIVAL(String nama, double harga) {
        super(nama, harga);
    }

    //@Override digunakan untuk menandakan bahwa metode berikutnya merupakan override 
    //dari metode dalam superclass. Dalam kasus ini, metode hitungHarga() di-override

    @Override
    public double hitungHarga() {
        return harga;
    }
}
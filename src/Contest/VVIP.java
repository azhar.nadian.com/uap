package Contest;

class VVIP extends TiketKonser {
    //Kelas VVIP dideklarasikan dan meng-extend kelas TiketKonser menggunakan kata kunci 
    //extends. Ini berarti bahwa kelas VVIP mewarisi properti dan metode dari kelas TiketKonser

    public VVIP(String nama, double harga) {
        super(nama, harga);
    }

    //@Override digunakan untuk menandakan bahwa metode berikutnya merupakan override 
    //dari metode dalam superclass. Dalam kasus ini, metode hitungHarga() di-override

    @Override
    public double hitungHarga() {
        return harga;
    }
}
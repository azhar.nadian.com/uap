package Contest;

interface HargaTiket {
    //Metode hitungHarga() adalah sebuah metode tanpa implementasi yang bertujuan untuk menghitung 
    //harga tiket. Setiap kelas yang mengimplementasikan interface HargaTiket harus menyediakan 
    //implementasi konkret untuk metode ini. Tipe data yang dikembalikan oleh metode ini adalah double
    
    double hitungHarga();
}
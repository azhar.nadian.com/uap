package Contest;

class PemesananTiket {
    //array tiketKonser yang menyimpan objek-objek tiket konser yang tersedia dengan harganya
    
    private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 800000),
            new CAT1("CAT 1", 3500000),
            new FESTIVAL("FESTIVAL", 5000000),
            new VIP("VIP", 6000000),
            new VVIP("UNLIMITED EXPERIENCE", 11000000)
        };
    }

    //Metode pilihTiket(): Metode ini menerima parameter index yang digunakan untuk memilih tiket 
    //berdasarkan indeks di dalam array tiketKonser. Metode ini mengembalikan objek tiket yang 
    //sesuai dengan indeks yang diberikan.

    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}

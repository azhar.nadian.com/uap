package Contest;

class CAT1 extends TiketKonser {
    //Kelas CAT1 dideklarasikan dan meng-extend kelas TiketKonser menggunakan kata kunci 
    //extends. Ini berarti bahwa kelas CAT1 mewarisi properti dan metode dari kelas TiketKonser

    public CAT1(String nama, double harga) {
        super(nama, harga);
    }

    //@Override digunakan untuk menandakan bahwa metode berikutnya merupakan override 
    //dari metode dalam superclass. Dalam kasus ini, metode hitungHarga() di-override

    @Override
    public double hitungHarga() {
        return harga;
    }
}

package Contest;

class CAT8 extends TiketKonser {
    //Kelas CAT8 dideklarasikan dan meng-extend kelas TiketKonser menggunakan kata kunci 
    //extends. Ini berarti bahwa kelas CAt8 mewarisi properti dan metode dari kelas TiketKonser

    public CAT8(String nama, double harga) {
        super(nama, harga);
    }

    //@Override digunakan untuk menandakan bahwa metode berikutnya merupakan override 
    //dari metode dalam superclass. Dalam kasus ini, metode hitungHarga() di-override

    @Override
    public double hitungHarga() {
        return harga;
    }
}